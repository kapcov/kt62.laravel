<?php

use Illuminate\Database\Seeder;

class TheaterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $theater = new \App\Theater([
            'name' => 'dram',
            'title' => 'Театр Драмы',
            'link' => 'http://www.rzndrama.ru/ru/index.html',
            'phone' => '45-15-58',
            'sort' => 0
        ]);
        $theater->save();

        $theater = new \App\Theater([
            'name' => 'kront',
            'title' => 'Спектакли в кинотеатре «Кронверк»',
            'link' => 'http://www.theatrehd.ru/ru/schedule?city=17',
            'phone' => '',
            'sort' => 10
        ]);
        $theater->save();

        $theater = new \App\Theater([
            'name' => 'perehod',
            'title' => 'Театр «Переход»',
            'link' => 'http://przn.ru/poster',
            'phone' => '32-88-80',
            'sort' => 30
        ]);
        $theater->save();

        $theater = new \App\Theater([
            'name' => 'fil',
            'title' => 'Филармония',
            'link' => 'http://rznfilarmonia.ru/afisha/',
            'phone' => '28-05-56',
            'sort' => 40
        ]);
        $theater->save();

        $theater = new \App\Theater([
            'name' => 'puppet',
            'title' => 'Кукольный театр',
            'link' => 'http://rznpuppet.ru/%D0%B0%D1%84%D0%B8%D1%88%D0%B0/',
            'phone' => '45-54-26',
            'sort' => 50
        ]);
        $theater->save();

        $theater = new \App\Theater([
            'name' => 'tuz',
            'title' => 'ТЮЗ',
            'link' => 'http://www.rzn-tdm.ru/?mod=pages&id=1',
            'phone' => '27-52-38, 25-95-60',
            'sort' => 60
        ]);
        $theater->save();

        $theater = new \App\Theater([
            'name' => 'kmax',
            'title' => 'Спектакли в кинотеатре «Киномакс»',
            'link' => 'http://teatr-pro.ru/category/repertuar/',
            'phone' => '',
            'sort' => 20
        ]);
        $theater->save();
    }
}
