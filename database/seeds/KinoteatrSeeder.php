<?php

use Illuminate\Database\Seeder;

class KinoteatrSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kinoteatr = new \App\Kinoteatr([
            'name' => 'luxor',
            'title' => 'Люксор [Круиз]',
            'link' => 'http://www.luxorfilm.ru/cinema/ryazan/'
        ]);
        $kinoteatr->save();

        $kinoteatr = new \App\Kinoteatr([
            'name' => 'malina',
            'title' => 'Малина',
            'link' => 'http://www.malina-cinema.ru/'
        ]);
        $kinoteatr->save();

        $kinoteatr = new \App\Kinoteatr([
            'name' => 'luxor_bars',
            'title' => 'Люксор [Барс]',
            'link' => 'http://www.luxorfilm.ru/cinema/ryazan-bars/'
        ]);
        $kinoteatr->save();

        $kinoteatr = new \App\Kinoteatr([
            'name' => '5zvezd',
            'title' => '5 звёзд',
            'link' => 'http://www.5zvezd.ru/schedule/#3073:8'
        ]);
        $kinoteatr->save();

        $kinoteatr = new \App\Kinoteatr([
            'name' => 'kinomax',
            'title' => 'Киномакс',
            'link' => 'https://kinomax.ru/ryazan/'
        ]);
        $kinoteatr->save();

        $kinoteatr = new \App\Kinoteatr([
            'name' => 'kron',
            'title' => 'Кронверк',
            'link' => 'http://www.formulakino.ru/cinemas/rjazan/viktoria_plaza/'
        ]);
        $kinoteatr->save();
    }
}
