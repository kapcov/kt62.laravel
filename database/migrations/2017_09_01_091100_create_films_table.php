<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->integer('kpid')->default(0)->unsigned();
            $table->float('rating', 10, 1)->default(0);
            $table->integer('count')->default(0)->unsigned();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
        DB::statement('ALTER TABLE films ADD FULLTEXT full(title)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
