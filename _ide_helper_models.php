<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\Film
 *
 * @property-read \App\Seance $seance
 */
	class Film extends \Eloquent {}
}

namespace App{
/**
 * App\Shedule
 *
 * @property-read \App\Theater $theater
 */
	class Shedule extends \Eloquent {}
}

namespace App{
/**
 * App\Seance
 *
 * @property-read \App\Film $film
 * @property-read \App\Kinoteatr $kinoteatr
 */
	class Seance extends \Eloquent {}
}

namespace App{
/**
 * App\Theater
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Shedule[] $shedule
 */
	class Theater extends \Eloquent {}
}

namespace App{
/**
 * App\Kinoteatr
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Seance[] $seans
 */
	class Kinoteatr extends \Eloquent {}
}

