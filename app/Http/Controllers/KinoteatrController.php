<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kinoteatr;

class KinoteatrController extends Controller
{
    public function getIndex()
    {
        $kinoteatr = Kinoteatr::with('seans.film')->get();
        return view('index', compact('kinoteatr'));
    }
}
