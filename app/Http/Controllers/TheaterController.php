<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Theater;

class TheaterController extends Controller
{
    public function getIndex()
    {
        $theaters = Theater::with('shedule.theater')->get();
        return view('theaters', ['theaters' => $theaters]);
    }
}
