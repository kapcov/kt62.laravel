<?php

namespace App\Http\Controllers;

use App\Kinoteatr;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Http\Request;
use Goutte\Client;
use App\Seance;
use App\Film;

class ApiController extends Controller
{
    public $stopTitle = [
        'Мульт в кино'
    ];

    public function getSeans($name)
    {
        $kt = Kinoteatr::where('name', $name)->first();
        $seans = $kt->seans()->get();
        return $seans;
    }

    public function getKinoteatrs()
    {
        return Kinoteatr::with('seans.film')->get();
    }

    public function getFilms()
    {
        // return Film::where('active', '=', 1)->get();
        // logger(Seance::distinct('title')->get()->toArray());
        return Seance::all();
    }

    public function getAll()
    {
        $this->getLuxor();
        $this->getLuxorBars();
        $this->getMalina();
        $this->getKinomax();
        $this->getKron();
        $this->get5zvezd();

        $this->copyFilmsToTable();
        $this->getKpData();
    }

    public function getLuxor()
    {
        $link = "http://www.luxorfilm.ru/cinema/ryazan/";
        $client = new Client();
        $crawler = $client->request('GET', $link);
        $result = array();
        $crawler->filter('table.cinema_container>tbody>tr')->each(function ($node) use (&$result) {
            // $title = $node->filter('td.seans-title h3>a>h3');
            $title = $node->filter('td.seans-title h3>a');
            $seans = array();
            $node->filter('td.cinema_time_info a.shedule-data')->each(function ($el) use (&$seans) {
                $ret = explode(' ', trim($el->text()), 2);
                $seans[] = trim($ret[0]);
            });
            $art = array(
                'title' => trim($title->text()),
                'seans' => implode(', ', $seans),
                'kinoteatr_name' => 'luxor'
            );
            $result[] = $art;
        });
        if (count($result) > 0) {
            Seance::where('kinoteatr_name', 'luxor')->delete();
        }
        foreach ($result as $item) {
            $data = new Seance($item);
            $data->save();
        }
        $this->copyFilmsToTable();
        return $result;
    }

    public function getLuxorBars()
    {
        $link = "http://www.luxorfilm.ru/cinema/ryazan-bars/";
        $client = new Client();
        $crawler = $client->request('GET', $link);
        $result = array();
        $crawler->filter('table.cinema_container>tbody>tr')->each(function ($node) use (&$result) {
            // $title = $node->filter('td.seans-title h3>a>h3');
            $title = $node->filter('td.seans-title h3>a');
            $seans = array();
            $node->filter('td.cinema_time_info a.shedule-data')->each(function ($el) use (&$seans) {
                $ret = explode(' ', trim($el->text()), 2);
                $seans[] = trim($ret[0]);
            });
            $art = array(
                'title' => trim($title->text()),
                'seans' => implode(', ', $seans),
                'kinoteatr_name' => 'luxor_bars'
            );
            $result[] = $art;
        });
        if (count($result) > 0) {
            Seance::where('kinoteatr_name', 'luxor_bars')->delete();
        }
        foreach ($result as $item) {
            $data = new Seance($item);
            $data->save();
        }
        $this->copyFilmsToTable();
        return $result;
    }

    public function getMalina()
    {
        $link = "https://widget.kassa.rambler.ru/schedulewidget/place/13814?WidgetID=21558&GeoPlaceID=2";
        $client = new Client();
        $crawler = $client->request('GET', $link);
        $result = array();

        $crawler->filter('#filterTable div.event.schedule-item>.event_in')->each(function ($node) use (&$result) {
            $title = $node->filter('h3');
            $seans = array();
            $node->filter('.event__schedule .btn__time')->each(function ($el) use (&$seans) {
                $seans[] = trim($el->text());
            });

            $art = array(
                'title' => trim($title->text()),
                'seans' => implode(', ', $seans),
                'kinoteatr_name' => 'malina'
            );
            $result[] = $art;
        });

        if (count($result) > 0) {
            Seance::where('kinoteatr_name', 'malina')->delete();
        }
        foreach ($result as $item) {
            $data = new Seance($item);
            $data->save();
        }
        $this->copyFilmsToTable();
        return $result;
    }

    public function getKron()
    {
        $link = "http://www.formulakino.ru/bitrix/templates/formulakino/ajax/cinema_schedule.php?CINEMA_ID=8694";
        $client = new Client();
        $crawler = $client->request('GET', $link);
        $result = array();
        $crawler->filter('div.cinemas div.item')->each(function ($node) use (&$result) {
            $active = $node->filter('span.date.active');
            if ($active->count() > 0) {
                $seans = array();
                $title = $node->filter('b.name a')->text();
                $node->filter('div.times span.date.active span.show320.active a')->each(function ($el) use (&$seans) {
                    $ret = preg_split("/[\s,]+/", trim($el->text()));
                    $seans[] = trim($ret[0]);
                });
                $art = array(
                    'title' => trim($title),
                    'seans' => implode(', ', $seans),
                    'kinoteatr_name' => 'kron'
                );
                $result[] = $art;
            }
        });
        if (count($result) > 0) {
            Seance::where('kinoteatr_name', 'kron')->delete();
        }
        foreach ($result as $item) {
            $data = new Seance($item);
            $data->save();
        }
        $this->copyFilmsToTable();
        return $result;
    }

    public function get5zvezd()
    {
//        $link = "http://www.5zvezd.ru/schedule/#3073:8";
        $link = "http://www.5zvezd.ru/ajax/scheduleFilter.php";
        $client = new Client();
        $params = array(
            'CITY_ID' => "3073",
            'CINEMA_ID' => "8",
            'DATE' => date("d.m.Y"),
            'FROM_HOUR' => "9",
            'TO_HOUR' => "2",
            'GENRES' => "0"
        );
        $crawler = $client->request('POST', $link, $params);
        $result = array();
        $crawler->filter('.sh_film')->each(function ($node) use (&$result) {
            $title = $node->filter('.sh_film_name h5 a')->text();
            $seans = array();
            $node->filter('.sh_film_cimena .sh_film_time span')->each(function ($el) use (&$seans) {
                $l_seans = str_replace("3D", "", $el->text());
                $sen = preg_replace('/\s\s+/', ', ', trim($l_seans));
                $sen = trim(str_replace("\n", ", ", $sen));
                if ($sen) {
                    $seans[] = trim($sen);
                }
            });
            $art = array(
                'title' => trim($title),
                'seans' => implode(', ', $seans),
                'kinoteatr_name' => '5zvezd'
            );
            $result[] = $art;
        });
        if (count($result) > 0) {
            Seance::where('kinoteatr_name', '5zvezd')->delete();
        }
        foreach ($result as $item) {
            $data = new Seance($item);
            $data->save();
        }
        $this->copyFilmsToTable();
        return $result;
    }

    public function getKinomax()
    {
        $link = "https://kinomax.ru/ryazan/";
        $client = new Client();
        $crawler = $client->request('GET', $link);

        $result = array();
        $crawler->filter('.container.cinema-schedule div.film')->each(function ($node) use (&$result) {
            $title = $node->filter('.w-70')->text();
            $seans = array();
            $node->filter('.w-100 .session-tag')->each(function ($el) use (&$seans) {
                $seans[] = $el->text();
            });
            asort($seans);
            $art = array(
                'title' => trim($title),
                'seans' => implode(', ', $seans),
                'kinoteatr_name' => 'kinomax'
            );
            if (count($seans) !== 0) {
                $result[] = $art;
            }
        });
        if (count($result) > 0) {
            Seance::where('kinoteatr_name', 'kinomax')->delete();
        }
        foreach ($result as $item) {
            $data = new Seance($item);
            $data->save();
        }
        $this->copyFilmsToTable();
        return $result;
    }

    public function copyFilmsToTable()
    {
        // проверить в таблице films наличие этих фильмов
        $seances = Seance::select('title')->where('film_id', 0)->distinct()->get()->toArray();
        foreach ($seances as $sean) {
            $film = Film::where('title', $sean['title'])->first();
            if ($film) {
                Seance::where('title', $sean['title'])->update(['film_id' => $film->id]);
            }
        }

        // занести если есть новые фильмы
        $films = Seance::select('title')->where('film_id', 0)->distinct()->get()->toArray();
        foreach ($films as $film) {
            // $kpid = $this->getFilmKpId($film['title']);
            $kpid = 0;
            $newFilm = array(
                'title' => $film['title'],
                'kpid' => $kpid
            );
            $data = new Film($newFilm);
            $data->save();
            Seance::where('title', $film['title'])->update(['film_id' => $data->id]);
        }
        return $films;
    }

    public function getKpData()
    {
        libxml_use_internal_errors(true);
        $films = Film::where('kpid', '>', 0)->where('active', '=', 1)->get();
        foreach ($films as $film) {
            $link = "http://rating.kinopoisk.ru/".$film->kpid.".xml";
            $xml = simplexml_load_file($link, 'SimpleXMLElement', LIBXML_NOWARNING);
            if (false !== $xml) {
                $rating = $xml->kp_rating;
                $count = $xml->kp_rating["num_vote"];
                $film->rating = $rating;
                $film->count = $count;
                $film->save();
            }
        }
        return ['count update' => $films->count()];
    }

    public function getFilmKpId($name)
    {
        $link = 'http://kparser.pp.ua/json/search/'.$name;
        $json = file_get_contents($link);
        $array = json_decode($json);
        if (empty($array->result)) {
            return 0;
        }
        $ret = $array->result[0]->id;
        foreach ($array->result as $item) {
            if ($item->most_wanted) {
                $ret = $item->id;
                break;
            }
        }
        return $ret;
    }

    public function getKpAfisha()
    {
        // $films = Film::where('kpid', '=', 0)->get();
        // print_r($films);
        // exit();
        $link = 'https://www.kinopoisk.ru/afisha/new/city/461/';
        $client = new Client();
        $crawler = $client->request('GET', $link);
        $result = [];
        $crawler->filter('.item .info .name')->each(function ($node) use (&$result) {
            $title = $node->filter('a')->text();
            $url = $node->filter('a')->attr('href');
            $kpid = substr($url, strrpos($url, '-') + 1);
            $kpid = str_replace('film', '', $kpid);
            $kpid = str_replace('/', '', $kpid);
            $result[] = compact('title', 'url', 'kpid');
        });
        $link = 'https://www.kinopoisk.ru/afisha/new/city/461/page/1/';
        $crawler = $client->request('GET', $link);
        $crawler->filter('.item .info .name')->each(function ($node) use (&$result) {
            $title = $node->filter('a')->text();
            $url = $node->filter('a')->attr('href');
            $kpid = substr($url, strrpos($url, '-') + 1);
            $kpid = str_replace('film', '', $kpid);
            $kpid = str_replace('/', '', $kpid);
            $result[] = compact('title', 'url', 'kpid');
        });
        // dd($result);
        foreach ($result as $item) {
            // $films = Film::where('kpid', '=', $item['kpid'])->get();
            // if (count($films) === 0) {
            //     $newFilm = array(
            //         'title' => $item['title'],
            //         'kpid' => $item['kpid']
            //     );
            //     $data = new Film($newFilm);
            //     $data->save();
            // }
            $query = "`kpid` = 0 AND MATCH (title) AGAINST ('". $item['title'] ."' IN BOOLEAN MODE)";
            $films = Film::whereRaw($query)->get();
            if (count($films) > 0) {
                $ids = [];
                foreach ($films as $film) {
                    $ids[] = $film['id'];
                }
                Film::whereIn('id', $ids)->update(['kpid' => $item['kpid']]);
            }
        }
        dd($result);
    }

    public function test()
    {
        $link = "https://kinomax.ru/ryazan/";
        $client = new Client();
        $crawler = $client->request('GET', $link);

        $result = array();
        $crawler->filter('.container.cinema-schedule div.film')->each(function ($node) use (&$result) {
            $title = $node->filter('.w-70')->text();
            $seans = array();
            $node->filter('.w-100 .session-tag')->each(function ($el) use (&$seans) {
                $seans[] = $el->text();
            });
            asort($seans);
            $art = array(
                'title' => trim($title),
                'seans' => implode(', ', $seans),
                'kinoteatr_name' => 'kinomax'
            );
            if (count($seans) !== 0) {
                $result[] = $art;
            }
        });
        // dd($result);
        if (count($result) > 0) {
            Seance::where('kinoteatr_name', 'kinomax')->delete();
        }
        foreach ($result as $item) {
            $data = new Seance($item);
            $data->save();
        }

        // $seans = [];
        //
        // $nodes = $crawler->filter('.container.cinema-schedule div.film');
        // if (count($nodes) > 0) {
        //     $nodes->each(function ($item) use (&$seans) {
        //         $title = $node->filter('.sh_film_name h5 a')->text();
        //         $seans = array();
        //         $node->filter('.sh_film_cimena .sh_film_time span')->each(function ($el) use (&$seans) {
        //             $l_seans = str_replace("3D", "", $el->text());
        //             $sen = preg_replace('/\s\s+/', ', ', trim($l_seans));
        //             $sen = trim(str_replace("\n", ", ", $sen));
        //             if ($sen) {
        //                 $seans[] = trim($sen);
        //             }
        //         });
        //         $art = array(
        //             'title' => trim($title),
        //             'seans' => implode(', ', $seans),
        //             'kinoteatr_name' => '5zvezd'
        //         );
        //         $result[] = $art;
        //         // $text = trim($item->text());
        //         // $text = substr($text, 0, 5);
        //         // if (!in_array($text, $seans)) {
        //         //     $seans[] = $text;
        //         // }
        //         // echo $item->text().'<br>';
        //     });
        //
        //     $title = $crawler->filter('h1')->text();
        //     $header = trim($crawler->filter('.schedule-header')->text());
        //     $result[$title] = [$seans, $header];
        // }
        // dd($result);

        // exit();


        // $crawler->filter('.table-schedule a.session-tag')->each(function ($node) use (&$seans) {
        //     if (!is_null($node)) {
        //         // var_dump($node);
        //         $text = trim($node->text());
        //         $text = substr($text, 0, 5);
        //         if (!in_array($text, $seans)) {
        //             $seans[] = $text;
        //         }
        //     }
        // });




        // $class_methods = get_class_methods(this);
        // dd($class_methods);
        $query = "`kpid` = 0 AND MATCH (title) AGAINST ('Гоголь. Начало' IN BOOLEAN MODE)";
        $products = Film::whereRaw($query)->get();

        $ids = [];
        foreach ($products as $item) {
            $ids[] = $item['id'];
        }

        dd($products, $ids);


        $films = Film::where('kpid', '=', 1047609)->get();
        if (count($films) === 0) {
            print "13321";
        }
        dd($films);
        // $link = 'http://www.kinopoisk.ru/index.php?first=yes&what=&kp_query=%D0%9E%D1%80%D0%B1%D0%B8%D1%82%D0%B0%209';
        // $link = 'http://kparser.pp.ua/json/search/чужой';
        $link = 'https://www.kinopoisk.ru/afisha/new/city/461/';
        // $json = file_get_contents($link);
        // var_dump($json);

        // $array = json_decode($json);
        // var_dump($array->result[0]->id);

        $client = new Client();
        $crawler = $client->request('GET', $link);
        // // $res = $crawler->filter('span.movie-ticket-button.feature_movie_ticket_button');
        // $res = $crawler->filter('.film-snippet')->first();
        // $res = $crawler->filter('.item .info .name')->first();
        $result = [];
        $crawler->filter('.item .info .name')->each(function ($node) use (&$result) {
            $title = $node->filter('a')->text();
            $url = $node->filter('a')->attr('href');
            $kpid = substr($url, strrpos($url, '-') + 1);
            $kpid = str_replace('/', '', $kpid);
            $result[] = compact($title, $url, $kpid);
        });
        print_r($result);
        exit();
        return ['satus' => 'test'];
    }

    public function deactivate()
    {
        $films = Film::with('seance')->where(['active' => 1])->get()->toArray();
        $ids = array();
        foreach ($films as $item) {
            if ($item['seance'] === null) {
                $ids[] = $item['id'];
            }
        }
        Film::whereIn('id', $ids)->update(['active' => 0]);
        return ['deactivate' => count($ids)];
    }
}
