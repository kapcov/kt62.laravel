<?php

namespace App\Http\Controllers;

use App\Shedule;
use Illuminate\Http\Request;
use Goutte\Client;

class ApiTeatrController extends Controller
{
    public function getShedule($name)
    {
        $obj = Theater::where('name', $name)->first();
        $shedule = $obj->shedule()->get();
        return $shedule;
    }

    private function parseDram($crawler)
    {
        $result = [];
        $nodes = $crawler->filter('td > div.cal_desc');
        if (count($nodes) > 0) {
            $oldDate = 1;
            $nodes->each(function ($node) use (&$result, &$oldDate) {
                $spect = $node->filter('p');
                $spect->each(function ($item) use (&$result, &$oldDate) {
                    $title = trim($item->filter('a')->text());
                    if (strpos($title, 'К сожалению, в этот день спектакли не запланированы') === false) {
                        $date = $item->filter('span')->text();
                        $split = preg_split('/, /', $date);
                        $date = $split[0];
                        $time = $split[1];
                        $url = $item->filter('a')->attr('href');
                        $art = [
                            'title' => $title,
                            'date' => $date,
                            'time' => $time,
                            'link' => $url,
                            'theater_name' => 'dram'
                        ];
                        if ($date >= $oldDate) {
                            $result[] = $art;
                            $oldDate = $date;
                        }
                    }
                });
            });
        }
        return $result;
    }

    public function getDram()
    {
        $post_array = array(
            'fYear' => date("Y"),
            'fMonth' => date("n"),
            'fOper' => 'curr',
            'fLang' => 'ru',
        );
        $link = "http://www.rzndrama.ru/kalendar.html";
        $client = new Client();
        $crawler = $client->request('POST', $link, $post_array);
        $currNodes = $this->parseDram($crawler);
        // next
        $post_array['fOper'] = 'next';
        $crawler = $client->request('POST', $link, $post_array);
        $nextNodes = $this->parseDram($crawler);

        $result = array_merge($currNodes, $nextNodes);
        // dd($result);

        Shedule::where('theater_name', 'dram')->delete();
        foreach ($result as $item) {
            $data = new Shedule($item);
            $data->save();
        }
        return $result;
    }

    public function getPerehod()
    {
        $link = "http://przn.ru/poster";
        $client = new Client();
        $crawler = $client->request('GET', $link);
        $result = [];
        $nodes = $crawler->filter('div.node-body div.content div.field-item ul li');
        if (count($nodes) > 0) {
            $nodes->each(function ($node) use (&$result) {
                $title = $node->filter('a')->text();
                $url = $node->filter('a')->attr('href');
                $url = "http://przn.ru".$url;
                $date = $node->html();
                $date = str_replace('<a href="/', '<a href="http://przn.ru/', $date);
                $art = ['title' => $title, 'comment' => $date, 'link' => $url, 'theater_name' => 'perehod'];
                $result[] = $art;
            });
        }
        Shedule::where('theater_name', 'perehod')->delete();
        foreach ($result as $item) {
            $data = new Shedule($item);
            $data->save();
        }
        return $result;
    }

    public  function getKron()
    {
        $link = "http://www.theatrehd.ru/ru/schedule?city=17";
        $result = [];
        $client = new Client();
        $crawler = $client->request('GET', $link);
        $nodes = $crawler->filter('div.schedule-day');
        if (count($nodes) > 0) {
            $nodes->each(function ($node) use (&$result) {
                $title = $node->filter('span.schedule-day-event-title')->text();
                $date = $node->filter('.schedule-day-date > strong')->text();
                $time = $node->filter('span.schedule-day-event-time')->text();
                $type = $node->filter('.schedule-day-event-info > span')->last()->text();
                $url = $node->filter('.schedule-day-event-image > a')->attr('href');
                $url = "http://www.theatrehd.ru".$url;
                // $img = $node->filter('.schedule-day-event-image img')->attr('src');

                $art = array('title' => $title, 'date' => $date, 'comment' => $type, 'time' => $time, 'link' => $url, 'theater_name' => 'kront');
                $result[] = $art;
            });
        }
        // dd($result);

        Shedule::where('theater_name', 'kront')->delete();
        foreach ($result as $item) {
            $data = new Shedule($item);
            $data->save();
        }
        return $result;
    }

    public  function getPuppet()
    {
        $link = "http://rznpuppet.ru/%D0%B0%D1%84%D0%B8%D1%88%D0%B0/";
        $result = [];
        $client = new Client();
        $crawler = $client->request('GET', $link);
        $nodes = $crawler->filter('div.afisha_one');
        if (count($nodes) > 0) {
            $nodes->each(function ($node) use (&$result) {
                $title = $node->filter('p.pagetitle_af')->text();
                if ($title !== '') {
                    $day = $node->filter('p.day_af')->text();
                    $month = $node->filter('p.month_af')->text();
                    $date = $day.' '.$month;
                    $time = $node->filter('p.time_af')->text();
                    $temp_url = $node->filter('.block2>a')->attr('href');
                    $url = "http://rznpuppet.ru/".$temp_url;
                    $art = array('title' => $title, 'date' => $date, 'time' => $time, 'link' => $url, 'theater_name' => 'puppet');
                    $result[] = $art;
                }
            });
        }
        // dd($result);

        Shedule::where('theater_name', 'puppet')->delete();
        foreach ($result as $item) {
            $data = new Shedule($item);
            $data->save();
        }
        return $result;
    }

    public  function getTuz()
    {
        $link = "http://www.rzn-tdm.ru/";
        $result = [];
        $client = new Client();
        $crawler = $client->request('GET', $link);
        $nodes = $crawler->filter('div.main_text table tr');
        if (count($nodes) > 0) {
            $nodes->each(function ($node) use (&$result) {
                // dump($node);
                try {
                    $td_date = $node->filter('td')->first();
                    $td_spect = $node->filter('td')->last();
                    $date = trim($td_date->filter('span')->text());
                    $title = trim($td_spect->filter('a')->text());
                    $time = trim($td_spect->filter('b')->text());
                    $url = "http://www.rzn-tdm.ru".trim($td_spect->filter('a')->attr('href'));
                    $art = array('title' => $title, 'date' => $date, 'time' => $time, 'link' => $url, 'theater_name' => 'tuz');
                    $result[] = $art;
                    // dd($date, $time, $title, $url);

                } catch (\Exception $err) {

                }
            });
        }
        // dd($result);

        Shedule::where('theater_name', 'puppet')->delete();
        foreach ($result as $item) {
            $data = new Shedule($item);
            $data->save();
        }
        return $result;
    }
}
