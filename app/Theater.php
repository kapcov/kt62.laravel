<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Theater extends Model
{
    protected $fillable = ['name', 'title', 'link', 'phone', 'sort'];

    public function shedule()
    {
        return $this->hasMany('App\Shedule', 'theater_name', 'name');
    }
}
