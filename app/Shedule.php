<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shedule extends Model
{
    protected $fillable = ['title', 'theater_name', 'comment', 'link', 'date', 'time'];

    public function theater()
    {
        return $this->belongsTo('App\Theater', 'theater_name', 'name');
    }
}
