<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seance extends Model
{
    protected $fillable = ['title', 'film_id', 'kinoteatr_name', 'seans'];

    public function film()
    {
        return $this->belongsTo('App\Film');
    }

    public function kinoteatr()
    {
        return $this->belongsTo('App\Kinoteatr', 'name', 'kinoteatr_name');
    }
}
