<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $fillable = ['title', 'kpid', 'rating', 'count', 'active'];

    public function seance()
    {
        return $this->hasOne('App\Seance');
    }
}
