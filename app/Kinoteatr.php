<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kinoteatr extends Model
{
    protected $fillable = ['name', 'title', 'link'];

    public function seans()
    {
        return $this->hasMany('App\Seance', 'kinoteatr_name', 'name');
    }
}
