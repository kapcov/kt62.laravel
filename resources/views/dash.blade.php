<?php
//     $class_methods = get_class_methods(\App\Http\Controllers\ApiController::class);
//     dd($class_methods);
?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <h2>kino</h2>
                        <ul>
                            <li>
                                <a href="{{ route('luxor') }}">luxor</a>
                            </li>
                            <li>
                                <a href="{{ route('luxor_bars') }}">luxor_bars</a>
                            </li>
                            <li>
                                <a href="{{ route('malina') }}">malina</a>
                            </li>
                            <li>
                                <a href="{{ route('kron') }}">kron</a>
                            </li>
                            <li>
                                <a href="{{ route('5zvezd') }}">5zvezd</a>
                            </li>
                            <li>
                                <a href="{{ route('kinomax') }}">kinomax</a>
                            </li>
                            <li>
                                <a href="{{ route('copyfilm') }}">copyfilm</a>
                            </li>
                            <li>
                                <a href="{{ route('getkpdata') }}">getkpdata</a>
                            </li>
                            <li>
                                <a href="{{ route('getkpafisha') }}">getkpafisha</a>
                            </li>
                            <li>
                                <a href="{{ route('test') }}">test</a>
                            </li>
                            <li>
                                <a href="{{ route('deactivate') }}">deactivate</a>
                            </li>
                        </ul>
                    </div>

                    <div class="panel-body">
                        <h2>Театр</h2>
                        <ul>
                            <li>
                                <a href="{{ route('dram') }}">dram</a>
                            </li>
                            <li>
                                <a href="{{ route('perehod') }}">perehod</a>
                            </li>
                            <li>
                                <a href="{{ route('kront') }}">kront</a>
                            </li>
                            <li>
                                <a href="{{ route('puppet') }}">puppet</a>
                            </li>
                            <li>
                                <a href="{{ route('tuz') }}">tuz</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection