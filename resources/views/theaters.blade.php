@extends('layouts.app')

@section('content')
    @foreach($theaters as $teatr)
        @php ($flagCollapse = false) @endphp
        <h3><a href="{{$teatr->link}}">{{$teatr->title}}</a>
            <small>{{$teatr->phone}}</small>
        </h3>
        <div class="well">
                @if($teatr->name == 'perehod')
                    @include('theaters.perehod')
                @elseif($teatr->name == 'kmax')
                    @include('theaters.kmax')
                @elseif($teatr->name == 'kront')
                    @include('theaters.kront')
                @elseif($teatr->name == 'tuz')
                    @include('theaters.tuz')
                @else
                    @include('theaters.main')
                @endif
        </div>
    @endforeach
@endsection
