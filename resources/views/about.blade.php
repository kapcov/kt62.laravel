@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Информация</div>

                    <div class="panel-body">
                        <p>Справочный сайт по кинотетрам и театрам Рязани.</p>
                        <p>Вся информация на сайте берётся с официальных сайтов организаций и обновляется ежедневно в автоматическом режиме.</p>
                        <p>Почта для вопросов и предложений <a href="mailto:info@kt62.ru">info@kt62.ru</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection