{{--@foreach($teatr->shedule as $key=>$item)--}}
{{--<li class="{{ $li_class }}">--}}
{{--{{ $item->date }} <span class="label {{ $label }}">{{ $item->time }}</span> — <a--}}
{{--href="{{$item->link}}">{{ $item->title }}</a>--}}
{{--@if($item->comment !== "")--}}
{{--<small>{{ $item->comment }}</small>--}}
{{--@endif--}}
{{--</li>--}}
{{--@endforeach--}}

<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"
                                              aria-expanded="true">Спектакли</a></li>
    <li role="presentation"><a href="#balet" aria-controls="balet" role="tab" data-toggle="tab" aria-expanded="false">Опера/Балет</a>
    </li>
</ul>
<br/>
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
        <ul class="list-unstyled">
            @foreach($teatr->shedule as $key=>$item)
                @if($item->comment === "спектакль")
                    <li>{{ $item->date }} <span class="label label-primary">{{ $item->time }}</span> — <a
                                href="{{$item->link}}">{{ $item->title }}</a>
                        <small>{{ $item->comment }}</small>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
    <div role="tabpanel" class="tab-pane" id="balet">
        <ul class="list-unstyled">
            @foreach($teatr->shedule as $key=>$item)
                @if($item->comment !== "спектакль")
                    <li>{{ $item->date }} <span class="label label-primary">{{ $item->time }}</span> — <a
                                href="{{$item->link}}">{{ $item->title }}</a>
                        <small>{{ $item->comment }}</small>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>