<ul class="list-unstyled">
    @foreach($teatr->shedule as $key=>$item)
        <li>{{ $item->date }} — <a href="{{$item->link}}">{{$item->title}}</a>
            @if($item->comment)
                | {{$item->comment}}
            @endif
        </li>
    @endforeach
</ul>