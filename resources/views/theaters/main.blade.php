<ul class="list-unstyled">
    @php
        $old_date = "";
        $li_class = "";
        $counter = 0;
        $mounts = config('app.mounts');
        $curMount = $mounts[date('m')];
    @endphp
    @foreach($teatr->shedule as $key=>$item)
        @if($teatr->name == 'dram' && mb_stripos($item->date, $curMount) > -1 && $item->date < date('d'))
            @continue
        @endif
        @php
            $counter++;
            $label = "label-info";
            if (substr($item->time, 0, strrpos($item->time, ":")) > 15) {
                $label = "label-primary";
            }
            $li_class = "";
            if ($old_date === $item->date) {
                $li_class = "no_date";
                $counter--;
            }
            $old_date = $item->date;
        @endphp
        <li class="{{ $li_class }}">
            {{ $item->date }} <span class="label {{ $label }}">{{ $item->time }}</span> — <a
                    href="{{$item->link}}">{{ $item->title }}</a>
            @if($item->comment !== "")
                <small>{{ $item->comment }}</small>
            @endif
        </li>
        @php ($resCount = count($teatr->shedule) - ($key - $counter + 1)) @endphp
        @if($counter === 10 && $resCount > 12 && !$flagCollapse)
            @php ($flagCollapse = true) @endphp
            </ul>
            <div class="panel-collapse collapse out" role="tabpanel" id="collapseListGroup{{$teatr->name}}"
                 aria-labelledby="collapseListGroupHeading{{$teatr->name}}" aria-expanded="true" style="">
                <ul class="list-unstyled">
        @endif
    @endforeach
</ul>

@if($flagCollapse)
    </div>
    <div class="panel-footer">
        <a href="#collapseListGroup{{$teatr->name}}" class="" role="button"
           data-toggle="collapse" aria-expanded="true"
           aria-controls="collapseListGroup{{$teatr->name}}">далее...</a>
    </div>
@endif