<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Кино-Театры Рязани | Афиша кинотеатров и театров Рязани</title>
    <meta name="keywords" content="кинотеатры Рязани, театры Рязани, афиша Рязани">
    <meta name="description" content="афиша кинотеатров и театров Рязани.">
    <meta name="google-site-verification" content="8nrg_2nqgu8I8brXr6Or0Tr9_ZHIb0fqRu4OG9Xywr0"/>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Кино-Театры Рязани
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                @php
                    $active = Request::segment(1);
                @endphp
                <ul class="nav navbar-nav">
                    <li class="{{ $active == '' ? 'active' : '' }}"><a href="{{ route('kino') }}">Кино</a></li>
                    <li class="{{ $active == 'theaters' ? 'active' : '' }}"><a href="{{ route('teatr') }}">Театр</a></li>
                    <li class="{{ $active == 'about' ? 'active' : '' }}"><a href="{{ route('about') }}">О нас</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @guest
                    {{--<li><a href="{{ route('login') }}">Login</a></li>--}}
                    {{--<li><a href="{{ route('register') }}">Register</a></li>--}}
                    @else
                        <li><a href="{{ route('dash') }}">Dash</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endguest
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        @yield('content')
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-8 text-center">
                <ul class="menu nav nav-pills">
                    <li class="first leaf {{ $active == '' ? 'active' : '' }}"><a href="/">Кино</a></li>
                    <li class="leaf {{ $active == 'theaters' ? 'active' : '' }}"><a href="/theaters">Театр</a></li>
                    <li class="last leaf {{ $active == 'about' ? 'active' : '' }}"><a href="/about">О сайте</a></li>
                    <li style="padding-left: 100px;"><a href="mailto:info@kt62.ru">info@kt62.ru</a></li>
                </ul>
            </div>
            <div class="col-xs-4" style="text-align: right; padding-top: 5px;">
                <!-- Yandex.Metrika counter -->
                <script type="text/javascript">
                    (function (d, w, c) {
                        (w[c] = w[c] || []).push(function () {
                            try {
                                w.yaCounter32652540 = new Ya.Metrika({
                                    id: 32652540,
                                    clickmap: true,
                                    trackLinks: true,
                                    accurateTrackBounce: true
                                });
                            } catch (e) {
                            }
                        });

                        var n = d.getElementsByTagName("script")[0],
                            s = d.createElement("script"),
                            f = function () {
                                n.parentNode.insertBefore(s, n);
                            };
                        s.type = "text/javascript";
                        s.async = true;
                        s.src = "https://mc.yandex.ru/metrika/watch.js";

                        if (w.opera == "[object Opera]") {
                            d.addEventListener("DOMContentLoaded", f, false);
                        } else {
                            f();
                        }
                    })(document, window, "yandex_metrika_callbacks");
                </script>
                <noscript>
                    <div><img src="https://mc.yandex.ru/watch/32652540" style="position:absolute; left:-9999px;"
                              alt=""/></div>
                </noscript>
                <!-- /Yandex.Metrika counter -->

                <!--LiveInternet counter-->
                <script type="text/javascript"><!--
                    document.write("<a href='//www.liveinternet.ru/click' " +
                        "target=_blank><img src='//counter.yadro.ru/hit?t14.11;r" +
                        escape(document.referrer) + ((typeof(screen) == "undefined") ? "" :
                            ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ?
                                screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) +
                        ";" + Math.random() +
                        "' alt='' title='LiveInternet: показано число просмотров за 24" +
                        " часа, посетителей за 24 часа и за сегодня' " +
                        "border='0' width='88' height='31'><\/a>")
                    //--></script><!--/LiveInternet-->
            </div>
        </div>
    </div>
</footer>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
