<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('seans/{name}', 'ApiController@getSeans');
Route::get('kinoteatrs', 'ApiController@getKinoteatrs');
Route::get('getfilms', 'ApiController@getFilms');

Route::get('luxor', 'ApiController@getLuxor')->name('luxor');
Route::get('luxor_bars', 'ApiController@getLuxorBars')->name('luxor_bars');
Route::get('malina', 'ApiController@getMalina')->name('malina');
Route::get('kron', 'ApiController@getKron')->name('kron');
Route::get('5zvezd', 'ApiController@get5zvezd')->name('5zvezd');
Route::get('kinomax', 'ApiController@getKinomax')->name('kinomax');
Route::get('copyfilm', 'ApiController@copyFilmsToTable')->name('copyfilm');
Route::get('getkpafisha', 'ApiController@getKpAfisha')->name('getkpafisha');
Route::get('getkpdata', 'ApiController@getKpData')->name('getkpdata');
Route::get('getfilmkpid/{name}', 'ApiController@getFilmKpId')->name('getfilmkpid');
Route::get('deactivate', 'ApiController@deactivate')->name('deactivate');
Route::get('test', 'ApiController@test')->name('test');

// theatre
Route::get('shedule/{name}', 'ApiTeatrController@getShedule');
Route::get('dram', 'ApiTeatrController@getDram')->name('dram');
Route::get('perehod', 'ApiTeatrController@getPerehod')->name('perehod');
Route::get('kront', 'ApiTeatrController@getKron')->name('kront');
Route::get('puppet', 'ApiTeatrController@getPuppet')->name('puppet');
Route::get('tuz', 'ApiTeatrController@getTuz')->name('tuz');
